#include "mbed.h"
 
DigitalOut ShiftClk(PC_13);        // klok schuifregister
DigitalOut ShiftEnable(PC_14);        // Enable output schuifregister
DigitalOut ShiftData(PC_15);        // data schuifregister
DigitalOut Display2(PA_0);          // display1 common annode
DigitalOut Display1(PA_1);          // display2 common annode
DigitalOut RE(PA_5);                //Active low RE SP3485EN (data in)
DigitalOut DE(PA_4);                //Active high DE SP3485EN (data out)
Serial     pc(PA_2, PA_3);          // TX & RX
 
int Waarden[12]= {0xC0,0xF9,0xA4,0xB0,0x99,0x92,0x83,0xF8,0x80,0x98,0xFF,0xBF}; // waarden 7 seg 0-9 + clear + --
 
int seg = 0;                                                // teller 7segment 1 of 2
int klokschuifregister = 0;                                 // klok schuifregister
int uitlezen = 0;                                           // teller aanzetten output enable schuifregister
int schuif = 0;                                             // aantal keer schuiven
 
int seg1 = 0;                                               // waarde segment1
int seg2 = 0;                                               // waarde segment2
 
char segwaarden[10] = {'0','1','2','3','4','5','6','7','8','9'}; 
 
Timer t;                                                    // functie timer
 
char karakter;                                              // variabele ingelezen karakters
int plaats = 0;                                             // variabele gebruikt om karakters in een array te plaatsen 
 
char commando [5];                                          // array die karakters opslaat om vergelijking te maken met commando's
 
void print(int data1, int data2) {                           // functie: data uitlezen schuifregister
 
    klokschuifregister = klokschuifregister^1;      // klokschuifregister aan-uit 
    ShiftClk = klokschuifregister;                 // klokschuifregister aansturen
            
    if(seg==0){                                     // display 1 aansturen
        ShiftData = (Waarden[data1] >> (7-schuif)) & 1; // Uit te sturen bit selecteren
 
        if (klokschuifregister == 1){               // Als klok hoog is 
            schuif++;                               // data volgende keer meer schuiven
            uitlezen++;                             // variabele verhogen om uit te lezen
        }                                
 
        if (uitlezen == 8){                     // na 8 klokpulsen data op de 7seg plaatsen 
            Display1 = 0;                            // display 1 aan 
            Display2 = 1;                            // display 2 uit 
 
            ShiftEnable = 1;                           // enable output schuifregister aanzetten
            uitlezen = 0;                        // reset variable uitlezen                       
            schuif = 0;                          // aantal bits schuiven reset
            seg = 1;                             // veranderen aansturing 7segment 2de segment
        }
        ShiftEnable = 0;                              // enable output schuifregister uitzetten
    }
 
    if(seg==1){                                     // display 2 aansturen
        ShiftData = (Waarden[data2] >> (7-schuif)) & 1; // Uit te sturen bit selecteren
 
        if (klokschuifregister == 1){                // Als klok hoog is
            schuif++;                                // data volgende keer meer schuiven
            uitlezen++;                              // variabele verhogen om uit te lezen
        }                              
 
        if (uitlezen == 8){                          // na 8 klokpulsen uitlezen
            Display1 = 1;                                // display 1 uit
            Display2 = 0;                                // display 2 aan
                   
            ShiftEnable = 1;                               // enable output schuifregister aanzetten
            uitlezen = 0;                            // reset variabele uitlezen 
            schuif = 0;                              // aantal bits schuiven reset
            seg = 0;                                 // veranderen aansturen 7segment 1ste segment
        }
        ShiftEnable = 0;                                      // enable output schuifregister uitzetten
    }
}
 
void lezen(){
 print(seg2, seg1);
 if(pc.readable()){                                      // kijken of er een karakter gelezen kan worden
        karakter = pc.getc();                               // karakter inlezen
 
        if(karakter == 'a'){                                // als het juiste adress is gegeven verdergaan
 
            plaats = 0;                                     // plaats op 0 zetten zodat we een array kunnen opvullen
 
        while (plaats < 2){                                 // 5 karakters inlezen       
 
            karakter = pc.getc();                           // karakter inlezen
 
            commando[plaats] = karakter;                    // karakter in array plaatsen
 
            plaats++;                                       // plaats in het array opschuiven
 
            }
            seg1 = commando[0]-0x30;                        // segment 1 op instelwaarde plaatsen
            seg2 = commando[1]-0x30;                        // segment 2 op instelwaarde plaatsen
            print(seg2, seg1);
        }
    }
}
 
int main(){
    RE = 0;
    DE = 0;
    t.start(); 
    while(true){
        lezen();
    }
}
