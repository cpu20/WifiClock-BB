//
// 7-segement display driver library
// geschikt voor een MAX7219 driver chip
//

/* User adjustable variables! */
var User = "Demo"; //Gebruikersnaam om in te loggen
var Pass = "Demo"; //Passwoord om in te loggen
var LogoutTijd = 300; //Tijd voor er automatisch uitgelogd wordt

var Running = "P8_46"; //De beaglebone is geïnitialiseerd
var Connected = "P8_45"; 
var UserLoggedIn = "P8_44"; //Aanduiden dat een gebruiker is ingelogd
var ClkRun = "P8_43"; //Aanduiden dat de klok loopt
var clkStop = "P8_42"; //Aanduiden dat de klok is gestopt
var clkUserStop = "P8_41"; //Aanduiden dat de klok is gestopt door de gebruiker
var clkShotStop = "P8_40"; //Aanduiden dat de kloks is gestopt door aflopen schotklok

// read in the BoneScript library
var exec = require('child_process').exec, child;
//child = exec ('sudo echo BB-UART4 > /sys/devices/bone_capemgr.*/slots');
//Een http server creëren, de functie handler zal requests afhandelen
var app = require('http').createServer(handler);
//Communicatie opzetten tussen de server en de html pagina via socket.io
var io = require('socket.io').listen(app);
//Een filesystem variabele aanmaken om files te kunnen inlezeen en schrijven
var fs = require('fs');
//De bonescript library oproepen
var b = require('bonescript');
//Serialport library oproepen en dit voor UART poort 4
var SerialPort = require("serialport").SerialPort;
var serialPort = new SerialPort('/dev/ttyO4', {
    baudrate: 9600,
}, false);

app.listen(80); //Define the listen port for the web server
// socket.io options go here
io.set('log level', 2);   // reduce logging - set 1 for warn, 2 for info, 3 for debug
io.set('browser client minification', true);  // send minified client
io.set('browser client etag', true);  // apply etag caching logic based on version number
console.log('Server running on: http://' + getIPAddress() + ':80');   //Print out IP address

//De pinnen definiëren
var sData  = "P9_18"; //7-segment Data pin
var sClock = "P9_22"; //7-segment Clock pin
var sLatch = "P9_17"; //7-segment Latch pin (Data binnenlezen)
var DE = "P9_15"; //RS485 Data enable (Data uitsturen)
var RE = "P9_12"; //RS485 Read enable (Data inlezen, actief laag)
var Buzzer = "P8_7"; //Buzzer pin
//Globale variabelen definiëren
var MyTimer, timeoutID;
var Time = 0;
var ScoreA = 0, ScoreB = 0, StartMinuten = 30, StartSeconden = 0, Connected = 0, Teller = 0, Actueel = 0, ack = 0, Resend = 0;
var CheckBuffer, previous = 0, Logon = 0, SendSucces = 0, SchotSeconden = 40, Status = 'pauze';

//De pinnen configureren als outputs
b.pinMode(sData,  b.OUTPUT);
b.pinMode(sClock, b.OUTPUT);
b.pinMode(sLatch, b.OUTPUT);
b.pinMode(DE, b.OUTPUT);
b.pinMode(RE, b.OUTPUT);
b.pinMode(Buzzer, b.OUTPUT);

//De pinnen initialiseren
b.digitalWrite(sData,  b.LOW);
b.digitalWrite(sClock, b.LOW);
b.digitalWrite(sLatch, b.HIGH);
b.digitalWrite(DE, b.LOW);
b.digitalWrite(RE, b.HIGH);
b.digitalWrite(Buzzer, b.LOW);

//Indicatie pinnen
b.pinMode(Running, b.OUTPUT);
b.pinMode(Connected, b.OUTPUT);
b.pinMode(UserLoggedIn, b.OUTPUT);
b.pinMode(ClkRun, b.OUTPUT);
b.pinMode(clkStop, b.OUTPUT);
b.pinMode(clkUserStop, b.OUTPUT);
b.pinMode(clkShotStop, b.OUTPUT);

b.digitalWrite(Running, b.HIGH);    //Aangeven dat de BBB is geïnitialiseerd
b.digitalWrite(Connected, b.LOW);   //Alle andere pinnen laag zetten om te starten
b.digitalWrite(UserLoggedIn, b.LOW);
b.digitalWrite(ClkRun, b.LOW);
b.digitalWrite(clkStop, b.HIGH); //Klok is gestopt bij opstart
b.digitalWrite(clkUserStop, b.LOW);
b.digitalWrite(clkShotStop, b.LOW);

/* @brief Functie voor het afhandelen van http requests
*  
*  Als er een http request is, zal deze functie
*  de gevraagde file inlezen en deze teruggeven
*  aan de pagina.
*
*  @param req de aangevraagde file
*  @param res is de response naar de html pagina
*
*/
function handler (req, res) {
  
  if(req.url == "/jquery.mobile-1.4.5.min.css"){ //Een aanvraag voor de jquery file
      fs.readFile('jquery.mobile-1.4.5.min.css', //De gevraagde file inlezen
      function (err, data) { //Callback functie
      if (err) { //Kijken of de file correct ingelezen is
        res.writeHead(500); //Als response error code 500 teruggeven
        return res.end('Error loading jquery.mobile-1.4.5.min.css'); //In de console printen dat er iets is fout gegaan
      }
      res.writeHead(200);
      res.end(data);
      });
  }
  
  if(req.url == "/jquery.mobile-1.4.5.min.js"){
      fs.readFile('jquery.mobile-1.4.5.min.js',
      function (err, data) {
      if (err) {
        res.writeHead(500);
        return res.end('Error loading jquery.mobile-1.4.5.min.js');
      }
      res.writeHead(200);
      res.end(data);
      });
  }
  
  if(req.url == "/jquery-1.12.0.js"){
      fs.readFile('jquery-1.12.0.js',
      function (err, data) {
      if (err) {
        res.writeHead(500);
        return res.end('Error loading jquery-1.12.0.js');
      }
      res.writeHead(200);
      res.end(data);
      });
  }
 
    if(req.url == "/Netwerk.html" && Logon == 1){ //Pagina enkel teruggeven als er correct is ingelogd
      AwakeClient();
      fs.readFile('Netwerk.html',
      function (err, data) {
      if (err) {
        res.writeHead(500);
        return res.end('Error loading Netwerk.html');
      }
      res.writeHead(200);
      res.end(data);
      });
  }
  
    if(req.url == "/Login.js"){
      fs.readFile('Login.js',    
      function (err, data) {
      if (err) {
        res.writeHead(500);
        return res.end('Error loading Login.js');
      }
      res.writeHead(200);
      res.end(data);
      });
  }
  
      if(req.url == "/Netwerk.js"){
      fs.readFile('Netwerk.js',   
      function (err, data) {
      if (err) {
        res.writeHead(500);
        return res.end('Error loading Netwerk.js');
      }
      res.writeHead(200);
      res.end(data);
      });
  }

  if(req.url == "/Settings.html" && Logon == 1){
      AwakeClient();
      fs.readFile('Settings.html',    
      function (err, data) {
      if (err) {
        res.writeHead(500);
        return res.end('Error loading Settings.html');
      }
      res.writeHead(200);
      res.end(data);
      });
  }
  
  if(req.url == "/MainClock.html" && Logon == 1){
     AwakeClient();
     fs.readFile('MainClock.html',    
     function (err, data) {
       if (err) {
         res.writeHead(500);
         return res.end('Error loading MainClock.html');
       }
       res.writeHead(200);
       res.end(data);
    });
  }
  
  if(req.url == "/Game.html" && Logon == 1){
     AwakeClient();
     Actueel = 1; //De actuele waarden moeten weergegeven worden op de displays
     fs.readFile('Game.html',    
     function (err, data) {
       if (err) {
         res.writeHead(500);
         return res.end('Error loading Game.html');
       }
       res.writeHead(200);
       res.end(data);
    });
  }
  
  if(req.url == "/Main.html" && Logon == 1){
    AwakeClient();
    console.log("Ingelogd: "+ Logon);
    
     Actueel = 1;
         fs.readFile('Main.html',
         
         function (err, data) {
           if (err) {
             res.writeHead(500);
             return res.end('Error loading Main.html');
           }
           res.writeHead(200);
           res.end(data);
        });
  }
 
  if(req.url == "/ShotClock.html" && Logon == 1){
     AwakeClient();
     fs.readFile('ShotClock.html',
     function (err, data) {
       if (err) {
         res.writeHead(500);
         return res.end('Error loading ShotClock.html');
       }
       res.writeHead(200);
       res.end(data);
    });
  }
 
  if(req.url == "/Login.html"){
     fs.readFile('Login.html',   
     function (err, data) {
       if (err) {
         res.writeHead(500);
         return res.end('Error loading Login.html');
       }
       res.writeHead(200);
       res.end(data);
    });
  }
  
  fs.readFile('TimeScore.html',  //Als er hiervoor niks is ingelezen geven we
     function (err, data) {      //deze pagina terug
       if (err) {
         res.writeHead(500);
         return res.end('Error loading TimeScore.html');
       }
       res.writeHead(200);
       res.end(data);
  });
}


/* @brief Functie voor het afhandelen van socket requests
*  
*  Deze functie zal alle data die via socket wordt teuggestuurd
*  ontvangen en verwerken. Ook zal de data nodig voor de html
*  pagina's verstuurd worden. Deze functie zal uitgevoerd
*  worden als er een connectie is van socket.io.
*
*  @param --none
*
*/
io.on('connection', function (socket) {
  var interval; //Variabele om data in een interval door te sturen
    
  console.log("Connected"); //Via socket.io kijken wanneer iemand geconnecteerd is
  Connected += 1; //Aanduiden dat er iemand geconnecteerd is
  socket.on('disconnect', function(){ //Als er gedisconnect wordt
      Connected -= 1;
      /*console.log("Disconnected");
      //Connected = 0; //Aanduiden dat er gedisconnecteerd is
      clearInterval(interval); //De interval data stopzetten
      Tim.pauze(); //De timer pauzeren
      SchotTimer.pauze();
       
      timeoutID = setTimeout(DisplayIP1, 4000); //Het ip wordt na 4seconden dat er geen connectie is weergegeven*/
  });
  // listen to sockets and write analog values to LED's
  socket.emit('HomeScore', ScoreA); //De socre's en de minuten terug naar de hmtl pagina's sturen om daar weer te geven
  socket.emit('AwayScore', ScoreB);
  socket.emit('Minutes', StartMinuten); //Beginwaarde doorsturen naar de html pagina voor de instellingen
  socket.emit('Seconds', StartSeconden);
  interval = setInterval(function(){ //De actuele waarden om de 500ms doorsturen naar de hmtl pagina
    socket.emit('Minuten', (Tim.GetMinutes() | 00)); //Actuele timer waarden naar de html sturen voor het displayen van de klok
    socket.emit('Seconden', Tim.GetSeconds());
    socket.emit('SchotSeconden', SchotTimer.GetSeconds() + (SchotTimer.GetMinutes()*60)); //Actuele schotklok seconden
    socket.emit('LogoutTimerMin', (LogoutTimer.GetMinutes() | 00)); //Actuele timer waarden naar de html sturen voor het displayen van de klok
    socket.emit('LogoutTimerSec', LogoutTimer.GetSeconds());
    socket.emit('HomeScoreUpdate', ScoreA); //De socre's en de minuten terug naar de hmtl pagina's sturen om daar weer te geven
    socket.emit('AwayScoreUpdate', ScoreB);
  }, 1000);
  socket.emit('ShotSeconden', SchotSeconden + (SchotTimer.GetMinutes()*60)); //De ingestelde shotclockseconden doorsturen voor de instellingen pagina
  
  socket.on('ShotSeconden', function(data){
      AwakeClient();
      SchotSeconden = data;
  });
  
  socket.on('Minuten', function (data) {    //Met socket.on de data van de html pagina's inlezen
  AwakeClient();
      StartMinuten = data; //Ingestelde start minuten
  });
  socket.on('Seconden', function (data){
      AwakeClient();
      StartSeconden = data; //Ingestelde start seconden
  });
  
  socket.on('ScoreHome', function (data) {
      AwakeClient();
      ScoreA = data; //De home score instellen
  });
  socket.on('ScoreAway', function (data) {
      AwakeClient();
      ScoreB = data; //De away score instellen
  });
  
  socket.on('TmrStrtStp', function (data) {
      AwakeClient();
    if (data == 'start') { //De timer starten of pauzeren
       b.digitalWrite(ClkRun, b.HIGH);
       b.digitalWrite(clkStop, b.LOW);
       b.digitalWrite(clkUserStop, b.LOW);
       b.digitalWrite(clkShotStop, b.LOW);
       Status = 'start';
       Tim.start();
       SchotTimer.start();
    } else if (data == 'stop') {
       b.digitalWrite(ClkRun, b.LOW);
       b.digitalWrite(clkStop, b.HIGH);
       b.digitalWrite(clkUserStop, b.HIGH);
       Status = 'pauze';
       Tim.pauze();
       SchotTimer.pauze();
    }
  });
  
  socket.on('TmrReset', function (data){
      AwakeClient();
      if(data == 'Rst'){ //De klok resetten
       b.digitalWrite(ClkRun, b.LOW);
       b.digitalWrite(clkStop, b.HIGH);
       b.digitalWrite(clkUserStop, b.HIGH);
       b.digitalWrite(clkShotStop, b.LOW);
       Tim.reset();
       SchotTimer.reset();
       Status = 'stop';
    
    } 
  });
  
  socket.on('SSID', function (ssid, pass){
      AwakeClient();
      WriteInterfaces(ssid, pass); //SSID en passwoord netwerk aanpassen
      console.log(ssid + pass);
      
  });
  
  socket.on('reboot', function(){
    reboot(); //De volledige wedstrijdklok resetten
  });
  
  socket.on('ShutDown', function(){
    ShutDown(); //De wedstrijdklok afsluiten (Linux wordt niet graag onverwachts afgesloten)
  });
  
  socket.on('ShotReset', function(data){
      AwakeClient();
    if(data == 'Rst') //De shotklok resetten zonder hme te pauzeren
        SchotTimer.reset();
    if(Status == 'start')
        SchotTimer.start();
  });

  socket.on('SetTimer', function(data){
     if (data == 1){ //Als er op set gedrukt wordt, zal de klok gereset worden om de nieuwe tijd
         AwakeClient();
         Tim.duration((StartMinuten*60)+StartSeconden);
         Tim.reset();//in te laden en weer te geven
     }
  });
  
  socket.on('SetSchot', function(data){
      if(data == 1){
          AwakeClient();
          SchotTimer.duration(SchotSeconden);
          SchotTimer.reset();
      }
  });
  
  socket.on('Logon', function(data){
      if((data == User + Pass) && Logon == 0){ //De gebruikersnaam en passwoord controleren voor het inloggen
          Logon = 1;
          socket.emit('Correct', 1);
          LogoutTimer.reset(); //De uitlog timer van 5min starten
          LogoutTimer.start();
          b.digitalWrite(UserLoggedIn, b.HIGH); //Aangeven dat de gebruiker is ingelogd
      }
      else if(Logon == 1)
          socket.emit('Correct', -1);
      else {
          socket.emit('Correct', 0);
      }
  });
  
  socket.on('Logout', function(data){
      if(data == 0){
          Logon = 0;
          clearInterval(interval); //De interval data stopzetten
          Tim.pauze(); //De timer pauzeren
          SchotTimer.pauze();
          timeoutID = setTimeout(DisplayIP1, 4000); //Het ip wordt na 4seconden dat er geen connectie is weergegeven*/  
          b.digitalWrite(UserLoggedIn, b.LOW); //Indicatie led uitzetten
          LogoutTimer.stop(); //Logout timer stoppen
          LogoutTimer.reset();
          //nu terugkeren naar het overzicht
      }
      console.log('Logon:' + Logon);
  });
  
  socket.on('ClientAwake', function(data){
      if(data == 1){
          AwakeClient();
      }
  });

  console.log(Connected); //Weer geven dat er geconnecteerd is
});

function AwakeClient(){
    LogoutTimer.reset();
    LogoutTimer.start();
}

//Initialisatie code
Init7seg(); //7-seg initialiseren
var Tim = new startTimer((StartMinuten*60)+StartSeconden, 'BeepLong'); //De timer initialiseren voor de hoofdklok
var SchotTimer = new startTimer(SchotSeconden, 'BeepShort'); //De timer initialiseren voor de schotklok
var LogoutTimer = new startLogoutTimer(LogoutTijd); //De timer voor auto-logout initialiseren
timeoutID = setTimeout(DisplayIP1, 4000); //Het IP beginnen weergeven
var RS485 = new RS485(); //RS485 variabele aanmaken
var MyScore = setInterval(UpdateDisplays, 100); //Het schrijven van de display activeren om de 10ms

/* @brief Functie om de user automatisch uit te loggen
*
*  @param duration de totale tijd om af te tellen in seconden
*  @return seconds het aantal seconden tot het einde van de timing
*  @return minutes het aantal minuten tot het einde van de timing
*
*/
function startLogoutTimer(duration){
    //In de variabele start stoppen we huidige systeemtijd
    var start = Date.now(), diff, minutes, seconds, StrtStp = 'reset', pauze;
    
    function timer(){ //De timer updaten
        diff = duration - (((Date.now() - start)/1000) | 0); //Het verschil tussen nu en het einde van de timing berekenen
        
        minutes = (diff / 60) | 0; //Omzetten naar minuten en seconden
        seconds = (diff % 60) | 0; //De | 0 is om kommagetallen weg te werken

        if(diff <= 0){ //De klok stoppen na afloop
            StrtStp = 'einde';
            Logon = 0;
            b.digitalWrite(UserLoggedIn, b.LOW); //De inidcatie led dat de gerbuiker is ingelogd uitzetten
        }
    };
    
    
    this.GetSeconds = function(){ //De seconden terugsturen op aanvraag
        if(StrtStp == 'run')
            timer(); //De bijgehouden tijd updaten
        if(StrtStp == 'reset')
            return (duration%60); //Enkel de seconden overhouden van de volledige tijd
            
        return seconds;
        
    }
    this.GetMinutes = function(){ //De minuten terugsturen op aanvraag
        if(StrtStp == 'run')
            timer();
        if(StrtStp == 'reset')
            return (duration/60 | 0); //Enkel de minuten overhouden
            
        return minutes;
    }
    
    this.start = function(){ //De timer starten
        if(StrtStp == 'reset'){ //Kijken of de timer in reset mode zit
            start = Date.now(); //De huidige tijd updaten
            StrtStp = 'run';
        } else if(StrtStp == 'pauze'){ //Kijken of de timer gepauzeerd was
            StrtStp = 'run';
            start = start + (Date.now() - pauze); //De tijd verstreken tijdens het pauzeren elimineren
        }
    }
    this.pauze = function(){ //De timer pauzeren
        if(StrtStp == 'run'){
            StrtStp = 'pauze';
            pauze = Date.now(); //De systeemtijd op het moment van pauzeren opslaan
            //Op deze manier kunnen we na het pauzeren deze tijd uit de timing wegwerken
            //Anders loopt de timer door (systeemtijd) tijdens het pauzeren
        }
    }
    this.stop = function(){
        StrtStp = 'einde';
    }
    this.reset = function(){ //De timer resetten
        start = Date.now();
        StrtStp = 'reset';
    }
    this.duration = function(data){
        duration = data;
    }
    
}

/* @brief Functie om de timing van de klok te regelen
*  
*  Deze functie zal de timer starten, stoppen en resetten.
*  Deze functie zal ook de huidige tijd begeven en
*  teruggeven naar het programma indien gevraagd. Dit wordt
*  gedaan aan de hand van de systeemklok van de BeagleBone.
*
*  @param duration de totale tijd om af te tellen in seconden
*  @return seconds het aantal seconden tot het einde van de timing
*  @return minutes het aantal minuten tot het einde van de timing
*
*/
function startTimer(duration, beep){
    //In de variabele start stoppen we huidige systeemtijd
    var start = Date.now(), diff, minutes, seconds, StrtStp = 'reset', pauze;
    
    function timer(){ //De timer updaten
        diff = duration - (((Date.now() - start)/1000) | 0); //Het verschil tussen nu en het einde van de timing berekenen
        
        minutes = (diff / 60) | 0; //Omzetten naar minuten en seconden
        seconds = (diff % 60) | 0; //De | 0 is om kommagetallen weg te werken
        
        
	if(diff == 10){ //Als de tijd nog maar 10 seconden is, moet er een enkele beep afgaan
	    setTimeout(SingleBeep, 50);
	}        

        if(diff <= 0){ //De klok stoppen na afloop
            StrtStp = 'einde';
            if(beep == 'BeepLong'){
                b.digitalWrite(clkStop, b.HIGH);
                setTimeout(BuzzerHigh, 50); //Een buzzer laten afgaan aan het einde van de timing
                SchotTimer.stop();
            }
            else if(beep == 'BeepShort'){
                b.digitalWrite(clkShotStop, b.HIGH);
                setTimeout(BuzzerShort, 50);
                Tim.pauze();
                StrtStp = 'reset';
            }
        }
    };
    
    
    this.GetSeconds = function(){ //De seconden terugsturen op aanvraag
        if(StrtStp == 'run')
            timer(); //De bijgehouden tijd updaten
        if(StrtStp == 'reset')
            return (duration%60); //Enkel de seconden overhouden van de volledige tijd
            
        return seconds;
        
    }
    this.GetMinutes = function(){ //De minuten terugsturen op aanvraag
        if(StrtStp == 'run')
            timer();
        if(StrtStp == 'reset')
            return (duration/60 | 0); //Enkel de minuten overhouden
            
        return minutes;
    }
    
    this.start = function(){ //De timer starten
        Actueel = 1; //De actuele waarden van de tijd op de displays weergeven
        if(StrtStp == 'reset'){ //Kijken of de timer in reset mode zit
            start = Date.now(); //De huidige tijd updaten
            StrtStp = 'run';
        } else if(StrtStp == 'pauze'){ //Kijken of de timer gepauzeerd was
            StrtStp = 'run';
            start = start + (Date.now() - pauze); //De tijd verstreken tijdens het pauzeren elimineren
        }
    }
    this.pauze = function(){ //De timer pauzeren
        if(StrtStp == 'run'){
            StrtStp = 'pauze';
            pauze = Date.now(); //De systeemtijd op het moment van pauzeren opslaan
            //Op deze manier kunnen we na het pauzeren deze tijd uit de timing wegwerken
            //Anders loopt de timer door (systeemtijd) tijdens het pauzeren
        }
    }
    this.stop = function(){
        StrtStp = 'einde';
    }
    this.reset = function(){ //De timer resetten
        Actueel = 0; //Het spel is stopgezet en gereset dus de ingestelde waarden weergeven
        start = Date.now();
        StrtStp = 'reset';
    }
    this.duration = function(data){
        duration = data;
    }
    
}

var Beep = 0; //Variabele beep om het aantal beeps bij te houden

//Twee functies die elkaar afwisselend oproepen om een buzzer te laten afgaan
function BuzzerHigh(){
    b.digitalWrite(Buzzer, b.HIGH); //De buzzer pin hoog brengen
    Beep++; //Het aantal beeps verhogen
    if(Beep>=5){ //Na 5 beeps geven we nog één lange beep van 1000ms
        setTimeout(BuzzerLow, 1000);
    }
    else //Na 250ms de functie BuzzerLow oproepen
        setTimeout(BuzzerLow, 250);
}

function BuzzerLow(){
    b.digitalWrite(Buzzer, b.LOW); //De buzzer pin laag brengen
    if(Beep<5) //Zolang er nog geen 5 beeps geweest zijn, roepen we BuzzerHigh weer op na 250ms
        setTimeout(BuzzerHigh, 250);
    else //Na 5 beeps ophouden en beep resetten op 0
        Beep = 0;
}

function BuzzerShort(){
    b.digitalWrite(Buzzer, b.HIGH);
    setTimeout(SingleBeep2, 1000);
}

function SingleBeep(){
    b.digitalWrite(Buzzer, b.HIGH); //De buzzer pin hoog brengen
    setTimeout(SingleBeep2, 250); //Na 250ms de buzzer pin weer laag brengen
}

function SingleBeep2(){
    b.digitalWrite(Buzzer, b.LOW); //De buzzer pin weer laag brengen
}

//Variabele om de data tijdelijk in op te slaan
var ResendData;

/* @brief Functie voor het verzenden van data over RS485
*  
*  Met deze functie kan er data verzonden worden via RS485.
*  Het karakter 'a' voor adressering wordt automatisch
*  aan de data toegevoegd.
*
*  @param data de data om te verzenden
*
*/
function RS485(){
    this.send = function(data){
        
        b.digitalWrite(DE ,b.HIGH); //De zend opamp van het RS485 IC inschakelen
        //b.digitalWrite(RE, b.LOW); //De verschilversterker ook actieveren om de data zelf weer in te lezen
        //CheckBuffer = ''; //CheckBuffer resetten om de eigen data in te lezen
        //console.log("\n\n"); //Debug informatie console
        //console.log("Starting transmission: " + data);
        ResendData = data; //De te zenden data tijdelijk opslaan voor later gebruik
        serialPort.write('a' + data, function () { //Via write, zenden we de data over de UART lijnen
            serialPort.drain(EndTransmission); //Nadat de data verzonden is zal EndTransmission worden opgeroepen
        });
    }
}

//Wordt opgeroepen na de data is verzonden op de UART lijnen
function EndTransmission(){
    b.digitalWrite(DE, b.LOW); //De zend opamp terug uitschakelen om de bus vrij te maken
    //setTimeout(CheckData, 20); //Check de verzonden data na 20ms
}

/* @brief Functie om de verzonden data te checken
*  
*  De functie zal de data die zonet is verzonden over RS485
*  effectief verzonden is. Als dit zo is, zal de verzonden
*  data ingelezen zijn en overeenstemmen met ResendData.
*  Is dit niet het geval, was de bus bezet en zal de
*  data opnieuw verzonden worden.
*
*  @param --none
*
*/
function CheckData(){
    if(SendSucces == 1){ //Als de data correct binnengelezen is melden we dit in de console
        console.log("Bus was free!" + CheckBuffer);
        previous = 0;
        SendSucces = 0; //Variabelen resetten
        CheckBuffer = '';
    }
    else{ //Data niet correct ingelezen, uitprinten in de console
        console.log("Bus was busy! Data was not send!");
    }
}

/* @brief Functie voor het updaten van de displays
*  
*  Functie die binnen een interval constant zal
*  opgeroepen worden om de informatie op de 
*  7-segment displays te updaten. Afhankelijk ofdat
*  de gebruiker geconnecteerd is, en of het spel bezig
*  is of niet zal de getoonde informatie verschillend
*  zijn.
*
*  @param --none
*
*/
function UpdateDisplays(){
    //Minuten en seconden opvragen
    var seconds = Tim.GetSeconds(), minutes = Tim.GetMinutes(), n = (SchotTimer.GetSeconds() + (SchotTimer.GetMinutes()*60));
    
    if (Connected > 0){ //Enkel weergeven als de gebruiker verbonden is
        Write7seg(7, (ScoreA/10));
        Write7seg(6, (ScoreA%10));
        Write7seg(1, (ScoreB/10));
        Write7seg(0, (ScoreB%10));
        if(Actueel == 1){ //Als te timer in gebruik is, geven we de actuele tijd weer
            Write7seg(5, (minutes/10));
            Write7seg(4, (minutes%10));
            Write7seg(3, (seconds/10));
            Write7seg(2, (seconds%10));
        }
        else if(Actueel == 0){ //Als de clock is gereset, moeten we de ingestelde waarden weergeven
            Write7seg(5, (StartMinuten/10));
            Write7seg(4, (StartMinuten%10));
            Write7seg(3, (StartSeconden/10));
            Write7seg(2, (StartSeconden%10));
        }
    }
    
    if(n<10)
        RS485.send("0" + n.toString());
    else
        RS485.send(n.toString());
}

/* @brief Functie om het begin van het IP aan te kondigen
*  
*  Dit zorgt ervoor dat het begin van het IP makkelijk kan
*  bepaald worden door de gebruiker.
*
*  @param --none
*
*/
function DisplayIP1(){
    if (Connected == 0){ //Controleren of er een connectie is of niet
        Write7seg(7, 0xF);
        Write7seg(6, 0xF);
        Write7seg(5, 0x1);
        Write7seg(4, 0xE); //Een P printen
        Write7seg(3, 0xF);
        Write7seg(2, 0xF);
        Write7seg(1, 0xF);
        Write7seg(0, 0xF);
        setTimeout(DisplayIP2, 2000); //De tweede functie oproepen voor het tweede deel van het IP weer te geven na 2s
    }
}

/* @brief Functie om het eerste deel van het IP weer te geven
*  
*  Deze functie zal het eerste deel van het IP adress
*  op de displays weergeven. Hiervoor wordt het IP
*  eerst van de '.' karakters ontdaan en opgesplitst
*  in afzonderlijke getallen.
*
*  @param --none
*
*/
function DisplayIP2(){
    var IPAdress = getIPAddress(); //Het IP ophalen vorm:"ABC.DEF.GHI.JKL"
    IPAdress = IPAdress.split('.'); //De puntjes weghalen vorm:"ABCDEFGHIJKL"
    var a = parseInt(IPAdress[0]), b = parseInt(IPAdress[1]); //Javascript zal die nu als
                                                              //een array behandlen: "ABC" "DEF" "GHI" "JKL"
                                                              //We kunnen dan de eerste twee getallen opslaan in a en b
    if (Connected == 0){ //Controleren of er een connectie is of niet
        Write7seg(7, (a/100));
        Write7seg(6, (a/10)%10);
        Write7seg(5, (a%10), 1);
        Write7seg(4, 10); //Twee streepjes printen
        Write7seg(3, 10);
        Write7seg(2, (b/100));
        Write7seg(1, (b/10)%10);
        Write7seg(0, (b%10), 1);
        setTimeout(DisplayIP3, 2000); //De tweede functie oproepen voor het tweede deel van het IP weer te geven na 2s
    }
}

/* @brief Functie om het tweede deel van het IP weer te geven
*  
*  Deze functie zal het tweede deel van het IP adress
*  op de displays weergeven. Hiervoor wordt het IP
*  eerst van de '.' karakters ontdaan en opgesplitst
*  in afzonderlijke getallen.
*
*  @param --none
*
*/
function DisplayIP3(){
    var IPAdress = getIPAddress();
    IPAdress = IPAdress.split('.');
    var c = parseInt(IPAdress[2]), d = parseInt(IPAdress[3]); //Nu het tweede en derde getal opslaan

    if (Connected == 0){ //De getallen printen
        Write7seg(7, (c/100));
        Write7seg(6, (c/10)%10);
        Write7seg(5, (c%10), 1);
        Write7seg(4, 10);
        Write7seg(3, 10);
        Write7seg(2, (d/100));
        Write7seg(1, (d/10)%10);
        Write7seg(0, (d%10), 1);
        setTimeout(DisplayIP1, 2000); //De eerste functie weer oproepen nan 2s
    }
}



//Zend Initialisatie commando's voor het 7 segement display
function Init7seg(){
    OutShift(0xC01); //Zorg ervoor dat het display niet in shutdown modus zit
    OutShift(0xF00); //Display test modus uitzetten
    OutShift(0xB07); //Alle 8 segmenten in gebruik nemen
    OutShift(0x9FF); //Voor alle displays de ingebouwde BCD-decoder inschakelen
    OutShift(0xA09); //De brightness van het display instellen
    //Zet alle digits op nul
    for(j=0;j<8;j++){
        Write7seg(j, 10);
    }
}

//Funcite om een getal naar een digit te sturen
//Input parameters: digit = de digit om op weer te geven
//                  data = getal om op de digit weer te geven
//Geen return parameters
function Write7seg(digit, data, DP){
    OutShift(((digit+1)<<8)|data|(DP<<7));
}

//Functie om 16bit met een clock naar buiten te sturen
function OutShift(data) {
    b.digitalWrite(sLatch, b.LOW); //De ChipSelect laag zetten
    for(i=0;i<16;i++){ //De 16bits naar buiten sturen
        if((data>>(15-i))&1){
            b.digitalWrite(sData, b.HIGH);
        }
        else{
            b.digitalWrite(sData, b.LOW);
        }
        b.digitalWrite(sClock, b.HIGH); //Een klokpuls sturen
        b.digitalWrite(sClock, b.LOW);
    }
    b.digitalWrite(sLatch, b.HIGH); //De ChipSelect terug hoog zetten om de data te verwerken
}

// Get server IP address on LAN
function getIPAddress() {
  var interfaces = require('os').networkInterfaces();
  for (var devName in interfaces) {
    var iface = interfaces[devName];
    for (var i = 0; i < iface.length; i++) {
      var alias = iface[i];
      if (alias.family === 'IPv4' && alias.address !== '127.0.0.1' && !alias.internal)
        return alias.address;
    }
  }
  return '0.0.0.0';
}

/* @brief Functie die opgeroepen wordt bij het ontvangen van data
*  
*  Als er data ontvangen wordt op de UART lijnen, zal deze functie
*  opgeroepen wroden. Deze zal dan kijken of de ontvangen data
*  overeen komt met de verzonden data. Indien dit het geval is,
*  zal de variabele SendSucces op één geplaatst worden.
*
*  @param --none
*
*/
serialPort.on('data', function(data) {
  console.log('data received: ' + data);
        
    /*if(previous == 0){      //Via previous zal data die in twee maal wordt ingelzen,
        CheckBuffer = data; //aan elkaar geplakt worden.
        previous++;
        console.log("First: " + CheckBuffer);
    }
    else{
        CheckBuffer = CheckBuffer + data; //Het tweede deel aan het eerste plakken
        previous = 0; //Previous resetten
        console.log("Second: " + CheckBuffer);
    }
    //Als de ontvangen data overeenkomt met één van deze variabelen, dan is de data succesvol verzonden
    if(CheckBuffer == "astart" || CheckBuffer == "apauze" || CheckBuffer == "areset" || data == "astart"
       || data == "apauze" || data == "areset"){
        previous = 0; //Variabelen resetten
        CheckBuffer = '';
        SendSucces = 1; //Aanduiden dat de data succesvol is verzonden
    }*/
});

//Als er error plaatsvind op de seriële poort, printen we dit in de console
serialPort.on('error',function(){
    console.log("Error serialport");
});

/* @brief Fucntie voor het openen van de seriële poort
*  
*  Deze functie zal de seriële poort openen. Na het
*  succesvol openen van de poort zal de shotklok op
*  initiële waarden worden ingesteld.
*  Indien de poort niet kan geopent worden, zal dit
*  in de console weergegeven worden.
*
*  @param --none
*
*/
serialPort.open(function (error) {
  if ( error ) {
    console.log('failed to open: '+error);
  }
  else{
    console.log('open');
  }

});

/* @brief Functie voor het herschrijven van de interfaces file
*  
*  De interfaces file zal in linux gebruikt worden om met
*  een netwerk te verbinden. Door het SSID en paswoord hierin
*  aan te passen kunnen we met een ander netwerk verbinden.
*  Alle tekst die niet aangepast wordt, staat in het groen
*  en zal net zoals het origineel teruggeschreven worden.
*
*  @param ssid het ssid van het netwerk
*  @param passwd het paswoord om met het netwerk te verbinden
*
*/
function WriteInterfaces(ssid, passwd){
    var stream = fs.createWriteStream("/etc/network/interfaces"); //Met het filesystem openen we de file
    stream.once('open', function(fd) { //Er zal in één lange stream de volledige file geschreven worden vanaf het begin
      stream.write("# This file describes the network interfaces available on your system\n");
      stream.write("# and how to activate them. For more information, see interfaces(5).\n");
      stream.write("\n");
      stream.write("# The loopback network interface\n");
      stream.write("\n");
      stream.write("auto lo\n");
      stream.write("iface lo inet loopback\n");
      stream.write("\n");
      stream.write("# The primary network interface\n");
      stream.write("#auto eth0\n");
      stream.write("#iface eth0 inet dhcp\n");
      stream.write("# Example to keep MAC address between reboots\n");
      stream.write("#hwaddress ether DE:AD:BE:EF:CA:FE\n");
      stream.write("\n");
      stream.write("# The secondary network interface\n");
      stream.write("#auto eth1\n");
      stream.write("#iface eth1 inet dhcp\n");
      stream.write("\n");
      stream.write("# WiFi Example\n");
      stream.write("auto ra0\n");
      stream.write("iface ra0 inet dhcp\n");
      stream.write("    wpa-ssid " + '"' + ssid + '"' + "\n");
      stream.write("    wpa-psk " + '"' + passwd + '"' + "\n");
      stream.write("\n");
      stream.write("# Ethernet/RNDIS gadget (g_ether)\n");
      stream.write("# ... or on host side, usbnet and random hwaddr\n");
      stream.write("# Note on some boards, usb0 is automaticly setup with an init script\n");
      stream.write("iface usb0 inet static\n");
      stream.write("    address 192.168.7.2\n");
      stream.write("    netmask 255.255.255.0\n");
      stream.write("    network 192.168.7.0\n");
      stream.write("    gateway 192.168.7.1\n");
      stream.end(); //Het schrijven van de stream naar de file stoppen
    });
}

//Functie om de BeagleBone te herstarten
function reboot() {
    child = exec('sudo reboot'); //Via exec voeren we vanuit javascript een "command line" commando uit
    
}

//Funcite voor het afsluiten van de BeagleBone
function ShutDown(){
    child = exec('sudo shutdown -h now');
}