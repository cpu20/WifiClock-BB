Wi-Fi wedstrijdklok
-------------------

Deze repository bevat de code voor een wedstrijdklok bediend aan de hand van Wi-Fi.
De code is geschreven voor de BeagleBone (Black en Green). De server voor het bedienen met Wi-Fi zal gedraaid worden op de BeagleBone zelf.
De main code is geschreven in javascript en is terug te vinden in HtmlTimer.js. Alle HTML-files zijn voor het bedienen van de wedstrijdklok.
Alle code is getest en werkt.

Installatie
-----------

Voor de ingebruikname van de code moet cloud9 geïnstalleerd zijn op de BeagleBone. Alle standaard BeagleBone images bevatten cloud9 volledig geconfigureerd.
Hetzelfde geld voor bonescript. Indien deze niet zijn geïnstalleerd zal je eerst een tutorial hievoor moeten opzoeken op internet.
Voor alle zekerheid update bonescript:
```
npm install bonescript
```

#### Andere te installeren componenten:

 * node serialport
```
npm install -g serialport
```
 * socket.io
```
npm install -g socket.io
```

### Aan te maken scripts:

Eerst moet er een script aangemaakt worden om UART poort 4 op de BeagleBone bij opstart actief te maken.
 * In Linux ga naar de home directory
```
cd /home
```
 * Maak een nieuw script aan met de naam startuart
```
sudo nano startuart
```
 * Vul volgende code in:
```
#!/bin/sh -e
slots=$(ls /sys/devices/bone_capemgr.*/slots)
echo BB-UART4 > $slots
exit
```
 * Druk CTRL+x om de file te sluiten en geef 'y' in om de file op te slaan
 * Ga nu naar de map /lib/systemd/system
```
cd /lib/systemd/system
```
 * Maak een service script aan
```
sudo nano startuart.service
```
 * Vul volgende code in:
```
[Unit]
Description=AutoStartUart service
[Service]
WorkingDirectory=/home
ExecStart=/home/startuart
SyslogIdentifier=StartUart
Restart=on-failure
RestartSec=5
[Install]
WantedBy=multi-user.target
```
 * En activeer het script:
```
systemctl enable startuart.service
```
 * Het script zal nu automatisch uitgevoerd worden bij opstart
 * Moest dit niet lukken dan zou bij het starten van HtmlTimer.js er een melding in de console komen dat het openen van tty04 niet lukt

Een tweede opstartscript is dit om HtmlTimer.js automatisch te starten bij opstart. Zo start de wedstrijdklok altijd automatisch op.
 * In Linux ga naar /lib/systemd/system
```
cd /lib/systemd/system
```
 * Maak een service script aan "StartWedstrijdklok kan iedere naam zijn die je wil"
```
sudo nano StartWedstrijdklok.service
```
 * Vul volgende code in:
```
[Unit]
Description=Bonescript autorun
ConditionPathExists=/var/lib/cloud9
[Service]
WorkingDirectory=/var/lib/cloud9/User/'De naam van de map waar de code inzit'
EnvironmentFile=/etc/default/node
ExecStart=/usr/bin/node HtmlTimer.js
SyslogIdentifier=bonescript-autorun
[Install]
WantedBy=multi-user.target
```
 * En activeer het script:
```
systemctl enable StartWedstrijdklok.service
```
### Indien je poort 80 wil gebruiken:
Dit zal de beaglebone webpagina uitschakelen:
```
systemctl disable bonescript.service              
systemctl disable bonescript.socket
```
(Vergeet niet om in de JavaScript file de poort naar jouw gewenste poort om te zetten)

#### Cloud9:

 * Maak in de map cloud9 een nieuwe map User aan
 * Maak in de map User een nieuwe map aan met een naam naar keuze
 * Download alle files van github en steek ze in de nieuw aangemaakte map
 * Open nu HtmlTimer.js en als alle andere stappen doorlopen zijn kan je het programma testen

Wi-Fi dongle
------------
Voor de Wi-Fi dongle moet de interfaces configuratie file aangepast worden met volgend commando:
```
sudo nano /etc/network/interfaces
```
Vanaf lijn 18 staat een voorbeeld om Wi-Fi te configureren. Verander het deel onder #WiFi Example naar:
```
#WiFi Example
auto ra0
iface ra0 inet dhcp
  wpa-ssid "SSID van jouw netwerk"
  wpa-psk  "Het wachtwoord van jouw netwerk"
```

~~Voor het installeren van de Wi-Fi dongle UWN100, volg volgende installatie procedure:~~
~~http://www.logicsupply.com/media/resources/manuals/Tutorial_Installing-Compact-USB-Wifi-Adapter-on-BBB.pdf~~

~~Ga naar pagina 3 en volg de quick installation. Daarna zou de dongle moeten werken.~~

Kopiëren van de BeagleBone image op een SD-kaart
------------------------------------------------
Op deze website is alle informatie te vinden om dit te doen:
http://elinux.org/BeagleBone_Black_Extracting_eMMC_contents

Eenmaal dat de files op de sd-kaart staan en de partities in orde zijn moet je de SD-kaart gewoon insteken en de eerste led zal beginnen knipperen. Hierna staat een image op de SD-kaart die je op zoveel SD-kaarten kan schrijven zoals je wil en in gelijk welke BeagleBone steken.

Om de image naar het intern geheugen te schrijven zie volgende website:
http://elinux.org/Beagleboard:Debian_On_BeagleBone_Black

Bugs/Problemen
--------------

Bij enige bugs of problemen contacteer gerust.

Developers
----------

#### De code is geschreven door:
 * Sam Pil
 * Jason Schelstraete
 * Tijl Schepens

© 2016 Sam Pil, Jason Schelstraete, Tijl schepens
All rights reserved. No part of this publication may be reproduced, distributed, or transmitted in any form or by any means, including photocopying, recording, or other electronic or mechanical methods, without the prior written permission of the publisher, except in the case of brief quotations embodied in critical reviews and certain other noncommercial uses permitted by copyright law. For permission requests, write to the publisher.

